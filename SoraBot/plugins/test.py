##
#

from SoraBot.events import register
from SoraBot.helpers import input_str
from telethon import events

@register(pattern="^/teste")
async def test_(event):
    text = input_str(event)
    if not text:
        await event.reply("__Não funcionou__")
    await event.reply(text)