##
#
from .. import SoraX
from telethon import events, Button
from SoraBot.events import register

@register(pattern="^/start")
async def start(event):
    await event.reply("__Oi eu sou Sora\n\nPor enquanto não faço muita coisa.__",
                    buttons=[
                        [Button.url("Dev", url="https://t.me/fnixdev")],
                        [Button.inline("Curioso?",data="example")]
                    ])

@SoraX.on(events.callbackquery.CallbackQuery(data="example"))
async def ex(event):
    await event.edit("Isso é apenas um teste!")