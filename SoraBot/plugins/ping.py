from SoraBot.events import register
from telethon import events
from datetime import datetime

@register(pattern="^/pong")
async def start(ping):
    start = datetime.now()
    end = datetime.now()
    m_s = (end - start).microseconds / 1000
    await ping.reply(f"•PING: {m_s}ms")