##
#

import yt_dlp
import requests
import json

from youtubesearchpython import Search, SearchVideos
from telethon import events, Button
from .. import SoraX
from SoraBot.events import register
from SoraBot.helpers import input_str


@register(pattern="^/kek")
async def ytdl_(event):
    query = input_str(event)
    chat_id = event.chat_id
    if not query:
        await event.reply("__Bruhh..__")
    try:
        res_ = SearchVideos(query, offset=1, mode="json", max_results=1)
        results = json.loads(res_.result())["search_result"]
        link = results[0]["link"]
        title = results[0]["title"]
        thumbnail = results[0]["thumbnails"][3]
        thumb_name = f"thumb.jpg"
        thumb = requests.get(thumbnail, allow_redirects=True)
        open(thumb_name, "wb").write(thumb.content)
        duration_ = results[0]["duration"]
        channel_ = results[0]["channel"]
        views_ = results[0]["views"]
        capt = f"**[{title}]({link})**\n❯ Duração: {duration_}\n❯ Views: {views_}\n❯ Canal: {channel_}"
        but_ = [[
                Button.inline("🎶 Áudio", data="aud_"),
                Button.inline("🎬 Vídeo", data="vid_")
                ]]
    except Exception as e:
        await event.reply(f"`Som não encontrado\n\n{e}`")
        print(str(e))
        return
    await SoraX.send_file(chat_id, thumb_name, caption=capt, buttons=but_)