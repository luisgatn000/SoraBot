##
#
import requests
from telethon import events, Button
from .. import SoraX
from SoraBot.events import register
from SoraBot.helpers import input_str

@register(pattern="^/nekos")
async def start(event):
    but_ = [[Button.inline("Próximo",data="next_nek")]]
    r = requests.get("https://nekos.life/api/v2/img/neko")
    g = r.json().get("url")
    await SoraX.send_file(event.chat_id, g, buttons=but_)

@SoraX.on(events.callbackquery.CallbackQuery(data="next_nek"))
async def ex(event):
    r = requests.get("https://nekos.life/api/v2/img/neko")
    g = r.json().get("url")
    await event.edit(g)

@register(pattern="^/kanna")
async def kanna_(event):
    query = input_str(event)
    r = requests.get(f"https://nekobot.xyz/api/imagegen?type=kannagen&text={query}").json()
    pic = r.get("message")
    await SoraX.send_file(event.chat_id, pic)