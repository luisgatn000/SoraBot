import os
import sys
import subprocess
from telethon import events
from SoraBot.events import register

@register(pattern="^/rr")
async def restart_(event):
    await event.reply("__Reiniciando...__")
    os.execv(sys.executable, [sys.executable, "-m", "SoraBot"])

@register(pattern="^/update")
async def restart_(event):
    msg = await event.reply("__Atualizando...__")
    os.system("git pull")
    await msg.edit("__Reiniciando...__")
    os.execv(sys.executable, [sys.executable, "-m", "SoraBot"])