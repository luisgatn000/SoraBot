from telethon import events
from . import SoraX

def register(**args):
    """ Registers a new message. """
    pattern = args.get('pattern')

    r_pattern = r'^[/!]'

    if pattern is not None and not pattern.startswith('(?i)'):
        args['pattern'] = '(?i)' + pattern

    args['pattern'] = pattern.replace('^/', r_pattern, 1)

    def decorator(func):
        SoraX.add_event_handler(func, events.NewMessage(**args))
        return func
    return decorator

__version__ = "1.0.0@beta"